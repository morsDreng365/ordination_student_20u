package controller;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class ControllerTest {

	Patient patient;
	Laegemiddel laegemiddel;

	@Before
	public void setUp() throws Exception {
		patient = new Patient("121256-0512", "Jane Jensen", 63.4);
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	}

	//-----------------------------------OPRETPN---------------------------------------
	@Test
	//TRUE
	public void testOpretPNOrdinationTC1() {
		PN pn = Controller.opretPNOrdination(LocalDate.of(2021,1,1), LocalDate.of(2021, 1, 1), patient, laegemiddel, 1);

		assertNotNull(pn);

		Laegemiddel laegemiddelTest = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");

		assertEquals(LocalDate.of(2021,1,1), pn.getStartDen());
		assertEquals(LocalDate.of(2021,1,1), pn.getSlutDen());
		assertEquals(laegemiddel.getNavn(), pn.getLaegemiddel().getNavn());
		assertEquals(1, pn.getAntalEnheder(), 0.00001);
	}

	@Test
	//TRUE
	public void testOpretPNOrdinationTC2() {
		PN pn = Controller.opretPNOrdination(LocalDate.of(2021,2,1), LocalDate.of(2021, 2, 1), patient, laegemiddel, 1);

		assertNotNull(pn);

		assertEquals(LocalDate.of(2021,2,1), pn.getStartDen());
		assertEquals(LocalDate.of(2021,2,1), pn.getSlutDen());
		assertEquals(laegemiddel.getNavn(), pn.getLaegemiddel().getNavn());
		assertEquals(1, pn.getAntalEnheder(), 0.00001);
	}

	@Test
	//TRUE
	public void testOpretPNOrdinationTC3() {
		PN pn = Controller.opretPNOrdination(LocalDate.of(2022,1,1), LocalDate.of(2022, 1, 1), patient, laegemiddel, 10);

		assertNotNull(pn);


		assertEquals(LocalDate.of(2022,1,1), pn.getStartDen());
		assertEquals(LocalDate.of(2022,1,1), pn.getSlutDen());
		assertEquals(laegemiddel.getNavn(), pn.getLaegemiddel().getNavn());
		assertEquals(10, pn.getAntalEnheder(), 0.00001);
	}

	@Test
	//FALSE
	public void testOpretPNOrdinationTC4() {
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			PN pn = Controller.opretPNOrdination(LocalDate.of(2021,1,1), LocalDate.of(2021, 1, 1), patient, laegemiddel, 0);
		});

		String expectedMessage = "start Dato er efter slut Dato eller antal er negativ";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	//FALSE
	public void testOpretPNOrdinationTC5() {
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			PN pn = Controller.opretPNOrdination(LocalDate.of(2021,1,1), LocalDate.of(2021, 1, 1), patient, laegemiddel, -1);
		});

		String expectedMessage = "start Dato er efter slut Dato eller antal er negativ";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	//FALSE
	public void testOpretPNOrdinationTC6() {
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			PN pn = Controller.opretPNOrdination(LocalDate.of(2021,2,1), LocalDate.of(2021, 1, 1), patient, laegemiddel, 10);
		});

		String expectedMessage = "start Dato er efter slut Dato eller antal er negativ";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}


	//-----------------------------------OPRETFAST---------------------------------------
	@Test
	//TRUE
	public void testOpretDagligFastOrdinationTC1() {
		DagligFast dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2021,1 , 1), LocalDate.of(2021, 1, 2), 
				patient, laegemiddel, 0, 0, 0, 0);

		assertNotNull(dagligFast);
		assertEquals(LocalDate.of(2021,1,1), dagligFast.getStartDen());
		assertEquals(LocalDate.of(2021,1,2), dagligFast.getSlutDen());
		assertEquals(0 , dagligFast.doegnDosis(), 0.0001);
	}

	@Test
	//TRUE
	public void testOpretDagligFastOrdinationTC2() {
		DagligFast dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2021,1 , 1), LocalDate.of(2021, 1, 2), 
				patient, laegemiddel, 1, 0, 0, 0);

		assertNotNull(dagligFast);
		assertEquals(LocalDate.of(2021,1,1), dagligFast.getStartDen());
		assertEquals(LocalDate.of(2021,1,2), dagligFast.getSlutDen());
		assertEquals(1 , dagligFast.doegnDosis(), 0.0001);
	}

	@Test
	//TRUE
	public void testOpretDagligFastOrdinationTC3() {
		DagligFast dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2021,1 , 1), LocalDate.of(2021, 1, 2), 
				patient, laegemiddel, 1, 1, 0, 0);

		assertNotNull(dagligFast);
		assertEquals(LocalDate.of(2021,1,1), dagligFast.getStartDen());
		assertEquals(LocalDate.of(2021,1,2), dagligFast.getSlutDen());
		assertEquals(2 , dagligFast.doegnDosis(), 0.0001);
	}

	@Test
	//TRUE
	public void testOpretDagligFastOrdinationTC4() {
		DagligFast dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2021,1 , 1), LocalDate.of(2021, 1, 2), 
				patient, laegemiddel, 1, 1, 1, 0);

		assertNotNull(dagligFast);
		assertEquals(LocalDate.of(2021,1,1), dagligFast.getStartDen());
		assertEquals(LocalDate.of(2021,1,2), dagligFast.getSlutDen());
		assertEquals(3 , dagligFast.doegnDosis(), 0.0001);
	}

	@Test
	//TRUE
	public void testOpretDagligFastOrdinationTC5() {
		DagligFast dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2021,1 , 1), LocalDate.of(2021, 1, 2), 
				patient, laegemiddel, 1, 1, 1, 1);

		assertNotNull(dagligFast);
		assertEquals(LocalDate.of(2021,1,1), dagligFast.getStartDen());
		assertEquals(LocalDate.of(2021,1,2), dagligFast.getSlutDen());
		assertEquals(4 , dagligFast.doegnDosis(), 0.0001);
	}

	@Test
	//TRUE
	public void testOpretDagligFastOrdinationTC6() {
		DagligFast dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2021,1 , 2), LocalDate.of(2021, 1, 2), 
				patient, laegemiddel, 0, 0, 0, 0);

		assertNotNull(dagligFast);
		assertEquals(LocalDate.of(2021,1,2), dagligFast.getStartDen());
		assertEquals(LocalDate.of(2021,1,2), dagligFast.getSlutDen());
		assertEquals(0 , dagligFast.doegnDosis(), 0.0001);
	}

	@Test
	//FALSE
	public void testOpretDagligFastOrdinationTC7() {
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			DagligFast dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2021, 1, 2), LocalDate.of(2021, 1, 1), 
					patient, laegemiddel, 0, 0, 0, 0);
		});

		String expectedMessage = "start Dato er efter slut Dato";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}


	//-----------------------------------OPRETSKÆV---------------------------------------
	@Test
	//TRUE
	public void testOpretDagligSkaevOrdinationTC1() {
		LocalTime[] klokkeSlet = {LocalTime.of(8, 0), LocalTime.of(12, 00), LocalTime.of(18, 00), LocalTime.of(0, 0)};
		double[] antalEnheder = {1,1,1,1};

		DagligSkaev dagligSkaev = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 4),
				patient, laegemiddel, klokkeSlet, antalEnheder);

		assertNotNull(dagligSkaev);
		assertEquals(LocalDate.of(2021,1,1), dagligSkaev.getStartDen());
		assertEquals(LocalDate.of(2021,1,4), dagligSkaev.getSlutDen());
		assertEquals(4 , dagligSkaev.doegnDosis(), 0.0001);
	}

	@Test
	//TRUE
	public void testOpretDagligSkaevOrdinationTC2() {
		LocalTime[] klokkeSlet = {LocalTime.of(8, 0)};
		double[] antalEnheder = {1,};

		DagligSkaev dagligSkaev = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 4),
				patient, laegemiddel, klokkeSlet, antalEnheder);

		assertNotNull(dagligSkaev);
		assertEquals(LocalDate.of(2021,1,1), dagligSkaev.getStartDen());
		assertEquals(LocalDate.of(2021,1,4), dagligSkaev.getSlutDen());
		assertEquals(1 , dagligSkaev.doegnDosis(), 0.0001);
	}

	@Test
	//TRUE
	public void testOpretDagligSkaevOrdinationTC3() {
		LocalTime[] klokkeSlet = {LocalTime.of(8, 0), LocalTime.of(12, 00), LocalTime.of(18, 00), LocalTime.of(0, 0)};
		double[] antalEnheder = {1,1,1,1};

		DagligSkaev dagligSkaev = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 2, 4),
				patient, laegemiddel, klokkeSlet, antalEnheder);

		assertNotNull(dagligSkaev);
		assertEquals(LocalDate.of(2021,1,1), dagligSkaev.getStartDen());
		assertEquals(LocalDate.of(2021,2,4), dagligSkaev.getSlutDen());
		assertEquals(4 , dagligSkaev.doegnDosis(), 0.0001);
	}

	@Test
	//TRUE
	public void testOpretDagligSkaevOrdinationTC4() {
		LocalTime[] klokkeSlet = {LocalTime.of(8, 0), LocalTime.of(12, 00), LocalTime.of(18, 00), LocalTime.of(0, 0)};
		double[] antalEnheder = {1,1,1,1};

		DagligSkaev dagligSkaev = Controller.opretDagligSkaevOrdination(LocalDate.of(2020, 1, 1), LocalDate.of(2021, 1, 4),
				patient, laegemiddel, klokkeSlet, antalEnheder);

		assertNotNull(dagligSkaev);
		assertEquals(LocalDate.of(2020,1,1), dagligSkaev.getStartDen());
		assertEquals(LocalDate.of(2021,1,4), dagligSkaev.getSlutDen());
		assertEquals(4 , dagligSkaev.doegnDosis(), 0.0001);
	}

	@Test
	//FALSE
	public void testOpretDagligSkaevOrdinationTC5() {
		LocalTime[] klokkeSlet = {LocalTime.of(8, 0), LocalTime.of(12, 00), LocalTime.of(18, 00), LocalTime.of(0, 0)};
		double[] antalEnheder = {1,1,1,1};

		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			DagligSkaev dagligSkaev = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2020, 1, 4),
					patient, laegemiddel, klokkeSlet, antalEnheder);
		});

		String expectedMessage = "start Dato er efter slut Dato";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));

	}

	@Test
	//FALSE
	public void testOpretDagligSkaevOrdinationTC6() {
		LocalTime[] klokkeSlet = {LocalTime.of(8, 0)};
		double[] antalEnheder = {1,1};

		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			DagligSkaev dagligSkaev = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 1, 1), LocalDate.of(2021, 1, 4),
					patient, laegemiddel, klokkeSlet, antalEnheder);
		});

		String expectedMessage = "ikke lige mange klokkeslet til doser";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	@Test
	//FALSE
	public void testOpretDagligSkaevOrdinationTC7() {
		LocalTime[] klokkeSlet = {LocalTime.of(8, 0), LocalTime.of(12, 00), LocalTime.of(18, 00), LocalTime.of(0, 0)};
		double[] antalEnheder = {1,1,1,1};

		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			DagligSkaev dagligSkaev = Controller.opretDagligSkaevOrdination(LocalDate.of(2021, 2, 1), LocalDate.of(2021, 1, 4),
					patient, laegemiddel, klokkeSlet, antalEnheder);
		});

		String expectedMessage = "start Dato er efter slut Dato";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}


	//-----------------------------------PNAnvendt---------------------------------------

	@Test
	//TRUE
	public void testOrdinationPNAnvendtTC1() {
		PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, laegemiddel, 3);

		Controller.ordinationPNAnvendt(pn, LocalDate.of(2019, 2, 12));

		assertEquals(3, patient.getOrdinationer().get(0).samletDosis(), 0.0001);
	}

	@Test
	//TRUE
	public void testOrdinationPNAnvendtTC2() {
		PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, laegemiddel, 3);

		Controller.ordinationPNAnvendt(pn, LocalDate.of(2019, 2, 14));

		assertEquals(3, patient.getOrdinationer().get(0).samletDosis(), 0.0001);
	}

	@Test
	//FALSE
	public void testOrdinationPNAnvendtTC3() {
		PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, laegemiddel, 3);

		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			Controller.ordinationPNAnvendt(pn, LocalDate.of(2018, 2, 12));
		});

		String expectedMessage = "Dato er ikke inden for behandlingsperioden";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));

	}

	@Test
	//TRUE
	public void testOrdinationPNAnvendtTC4() {
		PN pn = Controller.opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, laegemiddel, 3);

		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			Controller.ordinationPNAnvendt(pn, LocalDate.of(2019, 2, 15));
		});

		String expectedMessage = "Dato er ikke inden for behandlingsperioden";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
	}

	//-----------------------------------ANBEFALETDOSIS---------------------------------------

	@Test
	//TRUE
	public void testAnbefaletDosisPrDoegnTC1() {
		patient = new Patient("121256-0512", "Jane Jensen", 24.0);

		double result = Controller.anbefaletDosisPrDoegn(patient, laegemiddel);

		assertEquals(laegemiddel.getEnhedPrKgPrDoegnLet(), result, 0.0001);
	}

	@Test
	//TRUE
	public void testAnbefaletDosisPrDoegnTC2() {
		patient = new Patient("121256-0512", "Jane Jensen", 40.0);

		double result = Controller.anbefaletDosisPrDoegn(patient, laegemiddel);

		assertEquals(laegemiddel.getEnhedPrKgPrDoegnNormal(), result, 0.0001);
	}

	@Test
	//TRUE
	public void testAnbefaletDosisPrDoegnTC3() {
		patient = new Patient("121256-0512", "Jane Jensen", 130.0);

		double result = Controller.anbefaletDosisPrDoegn(patient, laegemiddel);

		assertEquals(laegemiddel.getEnhedPrKgPrDoegnTung(), result, 0.0001);
	}


	@Test
	//FALSE
	public void testAnbefaletDosisPrDoegnTC4() {
		patient = new Patient("121256-0512", "Jane Jensen", 24.0);

		double result = Controller.anbefaletDosisPrDoegn(patient, null);

		assertEquals(0, result, 0.0001);
	}

	@Test
	//FALSE
	public void testAnbefaletDosisPrDoegnTC5() {
		patient = new Patient("121256-0512", "Jane Jensen", 24.0);

		double result = Controller.anbefaletDosisPrDoegn(patient, null);

		assertEquals(0, result, 0.0001);
	}

	@Test
	//FALSE
	public void testAnbefaletDosisPrDoegnTC6() {
		patient = new Patient("121256-0512", "Jane Jensen", 130.0);

		double result = Controller.anbefaletDosisPrDoegn(patient, null);

		assertEquals(0, result, 0.0001);
	}

	@Test
	//TRUE
	public void testAntalOrdinationerPrVægtPrLægemiddelTC1() {
	
		Controller.initStorage();
	
		int result = Controller.antalOrdinationerPrVægtPrLægemiddel(75, 85, Storage.getInstance().getAllLaegemidler().get(1));
		
		assertEquals(1, result, 0.0001);
	}
	

	@Test
	//FALSE
	public void testAntalOrdinationerPrVægtPrLægemiddelTC2() {
	
		Controller.initStorage();
		Exception exception = assertThrows(IllegalArgumentException.class, () -> {
			int result = Controller.antalOrdinationerPrVægtPrLægemiddel(85, 75, Storage.getInstance().getAllLaegemidler().get(1));
		});

		String expectedMessage = "VægtStart skal være mindre ned vægtSlut";
		String actualMessage = exception.getMessage();

		assertTrue(actualMessage.contains(expectedMessage));
		
	}
	
	@Test
	//FALSE
	public void testAntalOrdinationerPrVægtPrLægemiddelTC3() {
		Controller.initStorage();
		
		int result = Controller.antalOrdinationerPrVægtPrLægemiddel(75, 85, null);
		
		assertEquals(0, result, 0.0001);
	}
}
