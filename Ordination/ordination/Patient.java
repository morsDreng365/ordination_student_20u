package ordination;

import java.util.*;

public class Patient {
	
	private String cprnr;
	private String navn;
	private double vaegt;

	private ArrayList<Ordination> ordinationer = new ArrayList<>();

	public Patient(String cprnr, String navn, double vaegt) {
		this.cprnr = cprnr;
		this.navn = navn;
		this.vaegt = vaegt;
	}

	public String getCprnr() {
		return cprnr;
	}

	public String getNavn() {
		return navn;
	}

	public void setNavn(String navn) {
		this.navn = navn;
	}

	public double getVaegt() {
		return vaegt;
	}

	public void setVaegt(double vaegt) {
		this.vaegt = vaegt;
	}

	public void removeOrdination(Ordination ordination) {
		if (ordinationer.contains(ordination))
			ordinationer.remove(ordination);
	}

	public void addOrdination(Ordination ordination) {
		if (!ordinationer.contains(ordination))
			ordinationer.add(ordination);
	}

	public ArrayList<Ordination> getOrdinationer() {
		return new ArrayList<>(this.ordinationer);
	}

	@Override
	public String toString() {
		return navn + "  " + cprnr;
	}

	// -----------------------------------------------------

	// TODO: Metoder til at vedligeholde linket til Ordination
}
