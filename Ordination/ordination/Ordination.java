package ordination;

import java.time.*;
import java.time.temporal.*;

public abstract class Ordination {
	private LocalDate startDato;
	private LocalDate slutDato;

	private Laegemiddel laegemiddel;

	// TODO Link til Laegemiddel

	// TODO constructor

	public Ordination(LocalDate startDato, LocalDate slutDato, Patient patient) {
		this.startDato = startDato;
		this.slutDato = slutDato;
		patient.addOrdination(this);
	}

	public LocalDate getStartDen() {
		return startDato;
	}

	public LocalDate getSlutDen() {
		return slutDato;
	}

	public void setLaegemiddel(Laegemiddel laegemiddel) {
		if (laegemiddel != this.laegemiddel)
			this.laegemiddel = laegemiddel;
	}

	public Laegemiddel getLaegemiddel() {
		return this.laegemiddel;
	}

	/**
	 * Returner antal hele dage mellem startdato og slutdato (begge dage inklusive).
	 */
	public int antalDage() {
		return (int) ChronoUnit.DAYS.between(startDato, slutDato) + 1;
	}

	@Override
	public String toString() {
		return startDato.toString();
	}

	/**
	 * Returner den totale dosis, der er givet i den periode ordinationen er gyldig.
	 */
	public abstract double samletDosis();

	/**
	 * Returner den gennemsnitlige dosis givet pr dag i den periode ordinationen er
	 * gyldig.
	 */
	public abstract double doegnDosis();

	/**
	 * Returner ordinationstypen som en tekst.
	 */
	public abstract String getType();

	// -----------------------------------------------------

	// TODO: Metoder til at vedligeholde linket til lagemiddel.

}
