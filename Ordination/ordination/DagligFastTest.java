package ordination;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;

public class DagligFastTest {

	private Patient patient;
	private Laegemiddel laegemiddel;
	private DagligFast dagligFast;


	@Before
	public void setUp() throws Exception {
		patient = new Patient("121256-0512", "Jane Jensen", 63.4);
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	}

	@Test
	//TRUE
	public void testSamletDosisTC1(){
		dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
				patient, laegemiddel, 1, 1, 1, 1);

		double result = dagligFast.samletDosis();

		assertNotNull(dagligFast);
		assertEquals(12, result, 0.0001);
	}

	@Test
	//TRUE
	public void testSamletDosisTC2(){
		dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
				patient, laegemiddel, 10, 10, 10, 10);

		double result = dagligFast.samletDosis();

		assertNotNull(dagligFast);
		assertEquals(120, result, 0.0001);
	}

	@Test
	//TRUE
	public void testSamletDosisTC3(){
		dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
				patient, laegemiddel, 25, 25, 25, 25);

		double result = dagligFast.samletDosis();

		assertNotNull(dagligFast);
		assertEquals(300, result, 0.0001);
	}

	@Test
	//TRUE
	public void testSamletDosisTC4(){
		dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
				patient, laegemiddel, 50, 50, 50, 50);

		double result = dagligFast.samletDosis();

		assertNotNull(dagligFast);
		assertEquals(600, result, 0.0001);
	}
	
	@Test
	//FALSE
	public void testSamletDosisTC5(){
			dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
					patient, laegemiddel, -1, -1, -1, -1);
 
			
	assertEquals(DagligFast.class, dagligFast.getClass());
	
	}


	@Test
	//TRUE
	public void testDoegnDosisTC1() {
		dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
				patient, laegemiddel, 1, 1, 1, 1);

		double result = dagligFast.doegnDosis();

		assertNotNull(dagligFast);
		assertEquals(4, result, 0.0001);
	}
	
	@Test
	//TRUE
	public void testDoegnDosisTC2() {
		dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
				patient, laegemiddel, 10, 10, 10, 10);

		double result = dagligFast.doegnDosis();

		assertNotNull(dagligFast);
		assertEquals(40, result, 0.0001);
	}
	
	@Test
	//TRUE
	public void testDoegnDosisTC3() {
		dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
				patient, laegemiddel, 25, 25, 25, 25);

		double result = dagligFast.doegnDosis();

		assertNotNull(dagligFast);
		assertEquals(100, result, 0.0001);
	}
	
	@Test
	//TRUE
	public void testDoegnDosisTC4() {
		dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
				patient, laegemiddel, 50, 50, 50, 50);

		double result = dagligFast.doegnDosis();

		assertNotNull(dagligFast);
		assertEquals(200, result, 0.0001);
	}
	
	@Test
	//FALSE
	public void testDoegnDosisTC5(){
			dagligFast = Controller.opretDagligFastOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
					patient, laegemiddel, -1, -1, -1, -1);

			assertEquals(DagligFast.class, dagligFast.getClass());
	}
}
