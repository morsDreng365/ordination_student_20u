package ordination;

import java.time.*;

public class DagligFast extends Ordination {

	private Dosis[] doser = new Dosis[4];

	public DagligFast(LocalDate startDato, LocalDate slutDato, Patient patient) {
		super(startDato, slutDato, patient);
		
	}

	@Override
	public double samletDosis() {
		double result = 0;
		for (Dosis d : doser)
			if(d.getAntal() > 0)
			result += d.getAntal();
		return result * antalDage();
	}
	

	@Override
	public double doegnDosis() {
		double count = 0;
		for (Dosis d : doser) {
			count = count + d.getAntal();
		}
		return count;
	}

	@Override
	public String getType() {
		return new String("DagligFast");
	}

	public Dosis createDosis(LocalTime tid, double antal, int i) {
		if(0 <= i  && i < 4 && doser.length <= 4) {
			if(antal < 0)
				antal = 0;
			Dosis dosis = new Dosis(tid, antal);
			doser[i] = dosis;
			return dosis;
		}else {
			return null;
		}
	}

	public void removeDosis(int i) {
		doser[i] = null;
	}

	public Dosis[] getDoser() {
		return this.doser;
	}

}
