package ordination;

import java.time.*;
import java.time.temporal.*;
import java.util.*;

public class PN extends Ordination {

	private double antalEnheder;

	private ArrayList<LocalDate> GivetDatoer = new ArrayList<>();

	public PN(LocalDate startDato, LocalDate slutDato, Patient patient, double antalEnheder) {
		super(startDato, slutDato, patient);
		this.antalEnheder = antalEnheder;
	}

	public double getAntalEnheder() {
		return antalEnheder;
	}

	/**
	 * Registrer dagen givesDen, hvor der er givet en dosis af PN. Returner true,
	 * hvis givesDen er inden for ordinationens gyldighedsperiode. Returner false
	 * ellers, og datoen givesDen ignoreres.
	 */

	public boolean givDosis(LocalDate givesDen) {
		if (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen()) || givesDen.equals(getStartDen())
				|| givesDen.equals(getSlutDen())) {
			GivetDatoer.add(givesDen);
			return true;
		}
		return false;
	}

	/**
	 * Returner antal gange ordinationen er anvendt.
	 */
	public int getAntalGangeGivet() {
		return GivetDatoer.size();
	}

	@Override
	public double samletDosis() {
		if (antalEnheder < 0)
			return 0;
		return antalEnheder * GivetDatoer.size();
	}

	@Override
	public double doegnDosis() {
		if (antalEnheder < 1)
			return 0;
		if (GivetDatoer.size() <= 1)
			return 0;
		;
		double givetdoser = GivetDatoer.size() * antalEnheder;
		double dage = ChronoUnit.DAYS.between(GivetDatoer.get(0), GivetDatoer.get(GivetDatoer.size() - 1)) + 1;

		return (givetdoser / dage);
	}

	@Override
	public String getType() {
		return "PN";
	}
}
