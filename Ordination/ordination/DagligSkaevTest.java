package ordination;

import static org.junit.Assert.*;
import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {

	Patient patient;
	Laegemiddel laegemiddel;
	DagligSkaev dagligSkaev;

	@Before
	public void setUp() throws Exception {
		patient = new Patient("121256-0512", "Jane Jensen", 63.4);
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	}

	@Test
	//TRUE
	public void testSamletDosisTC1(){
		dagligSkaev = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient);

		dagligSkaev.createDosis(LocalTime.of(8, 0), 1);

		double result = dagligSkaev.samletDosis();

		assertNotNull(dagligSkaev);
		assertEquals(3, result, 0.0001);
	}

	@Test
	//TRUE
	public void testSamletDosisTC2(){
		dagligSkaev = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient);

		for (int i = 0; i < 25; i++) {
				dagligSkaev.createDosis(LocalTime.of(8, 0), 25);
		}
	

		double result = dagligSkaev.samletDosis();

		assertNotNull(dagligSkaev);
		assertEquals(1875, result, 0.0001);
	}

	@Test
	//TRUE
	public void testSamletDosisTC3(){
		dagligSkaev = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient);

		for (int i = 0; i < 50; i++) {
			dagligSkaev.createDosis(LocalTime.of(8, 0), 50);
	}
		double result = dagligSkaev.samletDosis();

		assertNotNull(dagligSkaev);
		assertEquals(7500, result, 0.0001);
	}

	@Test
	//FALSE
	public void testSamletDosisTC4(){
		dagligSkaev = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient);

		Dosis dosis = dagligSkaev.createDosis(LocalTime.of(8, 0), -1);

		assertNull(dosis);	
	}


	@Test
	//TRUE
	public void testDoegnDosisTC1() {
		dagligSkaev = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient);

		dagligSkaev.createDosis(LocalTime.of(8, 0), 1);
		
		double result = dagligSkaev.doegnDosis();

		assertNotNull(dagligSkaev);
		assertEquals(1, result, 0.0001);
	}

	@Test
	//TRUE
	public void testDoegnDosisTC2() {
		dagligSkaev = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient);

		dagligSkaev.createDosis(LocalTime.of(8, 0), 10);
		
		double result = dagligSkaev.doegnDosis();

		assertNotNull(dagligSkaev);
		assertEquals(10, result, 0.0001);
	}

	@Test
	//TRUE
	public void testDoegnDosisTC3() {
		dagligSkaev = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient);

		dagligSkaev.createDosis(LocalTime.of(8, 0), 25);
		
		double result = dagligSkaev.doegnDosis();

		assertNotNull(dagligSkaev);
		assertEquals(25, result, 0.0001);
	}

	@Test
	//TRUE
	public void testDoegnDosisTC4() {
		dagligSkaev = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient);

		dagligSkaev.createDosis(LocalTime.of(8, 0), 50);

		
		double result = dagligSkaev.doegnDosis();

		assertNotNull(dagligSkaev);
		assertEquals(50, result, 0.0001);
	}

	@Test
	//FALSE
	public void testDoegnDosisTC5(){
		dagligSkaev = new DagligSkaev(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient);

		Dosis dosis = dagligSkaev.createDosis(LocalTime.of(8, 0), -1);

		assertNull(dosis);	
	}
}
