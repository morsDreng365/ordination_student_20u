package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {
	
	private ArrayList<Dosis> doser = new ArrayList<>();

    public DagligSkaev(LocalDate startDato, LocalDate slutDato, Patient patient) {
		super(startDato, slutDato, patient);
	}

	@Override
	public double samletDosis() {
		double result = 0;
		for(Dosis d : doser) {
			result += d.getAntal();
		}
		return result * antalDage();
	}

	@Override
	public double doegnDosis() {
		double result = 0;
		for(Dosis d : doser) {
			result = result + d.getAntal();
		}
		return result;
	}

	@Override
	public String getType() {
		return new String("DagligSkaev");
	}
	
	public Dosis createDosis(LocalTime tid, double antal) {
		if(antal > 0) {
		Dosis dosis = new Dosis(tid, antal);
		doser.add(dosis);
		return dosis;
		}else
			return null;
		
	}
	
	public void removeDosis(Dosis dosis) {
		if(this.doser.contains(dosis))
			this.doser.remove(dosis);
	}
	
	public ArrayList<Dosis> getDoser() {
		return new ArrayList<Dosis>(doser);
	}
	
	
	
	
	
	
}
