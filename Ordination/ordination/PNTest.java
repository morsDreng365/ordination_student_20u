package ordination;

import static org.junit.Assert.assertEquals;

import java.time.*;

import org.junit.*;

public class PNTest {
	Patient patient;
	Laegemiddel laegemiddel;

	@Before
	public void setUp() throws Exception {
		patient = new Patient("121256-0512", "Jane Jensen", 63.4);
		laegemiddel = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
	}

	@Test
	public void testSamletDosis1() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 1);
		pn.givDosis(LocalDate.of(2019, 2, 12));
		assertEquals(1, pn.samletDosis(), 0.001);
	}

	@Test
	public void testSamletDosis2() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 25);
		for (int i = 0; i < 25; i++) {
			pn.givDosis(LocalDate.of(2019, 2, 12));
		}
		assertEquals(625, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testSamletDosis3() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 25);
		for (int i = 0; i < 50; i++) {
			pn.givDosis(LocalDate.of(2019, 2, 12));
		}
		assertEquals(1250, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testSamletDosis4() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 50);
		for (int i = 0; i < 50; i++) {
			pn.givDosis(LocalDate.of(2019, 2, 12));
		}
		assertEquals(2500, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testSamletDosis5() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 0);
		assertEquals(0, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testSamletDosisexecptions1() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, -1);

		assertEquals(0, pn.doegnDosis(), 0.001);

	}

	@Test
	public void testDoegnDosis() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 1);
		pn.givDosis(LocalDate.of(2019, 2, 12));

		pn.givDosis(LocalDate.of(2019, 2, 14));
		assertEquals(0.6666, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis1() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 25);
		for (int i = 0; i < 24; i++) {
			pn.givDosis(LocalDate.of(2019, 2, 12));
		}

		pn.givDosis(LocalDate.of(2019, 2, 14));
		assertEquals(208.333, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis2() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 50);
		for (int i = 0; i < 24; i++) {
			pn.givDosis(LocalDate.of(2019, 2, 12));
		}

		pn.givDosis(LocalDate.of(2019, 2, 14));
		assertEquals(416.666, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis3() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 50);
		for (int i = 0; i < 49; i++) {
			pn.givDosis(LocalDate.of(2019, 2, 12));
		}

		pn.givDosis(LocalDate.of(2019, 2, 14));
		assertEquals(833.333, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis4() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 0);
		pn.givDosis(LocalDate.of(2019, 2, 12));

		pn.givDosis(LocalDate.of(2019, 2, 14));
		assertEquals(0, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testDoegnDosis5() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 1);

		pn.givDosis(LocalDate.of(2019, 2, 14));
		assertEquals(0, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testDoegnDosisexecptions1() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 0);

		pn.givDosis(LocalDate.of(2019, 2, 14));
		assertEquals(0, pn.doegnDosis(), 0.001);

	}

	@Test
	public void testDoegnDosisexecptions2() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, -1);

		pn.givDosis(LocalDate.of(2019, 2, 14));
		assertEquals(0, pn.doegnDosis(), 0.001);
	}

	@Test
	public void testGivDosis12() {
		PN pn = new PN(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14), patient, 1);
		assertEquals(true, pn.givDosis(LocalDate.of(2019, 2, 12)));
	}

	@Test
	public void testGivDosis14() {
		PN pn = new PN(LocalDate.of(2019, 2, 13), LocalDate.of(2019, 2, 14), patient, 1);
		assertEquals(true, pn.givDosis(LocalDate.of(2019, 2, 14)));
	}

	@Test
	public void testGivDosis13() {
		PN pn = new PN(LocalDate.of(2019, 2, 13), LocalDate.of(2019, 2, 14), patient, 1);
		assertEquals(true, pn.givDosis(LocalDate.of(2019, 2, 13)));
	}

	@Test
	public void testGivDosis11() {
		PN pn = new PN(LocalDate.of(2019, 2, 13), LocalDate.of(2019, 2, 14), patient, 1);
		assertEquals(false, pn.givDosis(LocalDate.of(2019, 2, 11)));
	}

	@Test
	public void testGivDosis15() {
		PN pn = new PN(LocalDate.of(2019, 2, 13), LocalDate.of(2019, 2, 14), patient, 1);
		assertEquals(false, pn.givDosis(LocalDate.of(2019, 2, 15)));
	}

	@Test
	public void testGivDosis3() {
		PN pn = new PN(LocalDate.of(2019, 2, 13), LocalDate.of(2019, 2, 14), patient, 1);
		assertEquals(false, pn.givDosis(LocalDate.of(2019, 3, 12)));
	}

	@Test
	public void testGivDosis1() {
		PN pn = new PN(LocalDate.of(2019, 2, 13), LocalDate.of(2019, 2, 14), patient, 1);
		assertEquals(false, pn.givDosis(LocalDate.of(2019, 1, 12)));
	}

	@Test
	public void testGivDosis18() {
		PN pn = new PN(LocalDate.of(2019, 2, 13), LocalDate.of(2019, 2, 14), patient, 1);
		assertEquals(false, pn.givDosis(LocalDate.of(2018, 2, 12)));
	}

	@Test
	public void testGivDosis20() {
		PN pn = new PN(LocalDate.of(2019, 2, 13), LocalDate.of(2019, 2, 14), patient, 1);
		assertEquals(false, pn.givDosis(LocalDate.of(2020, 2, 12)));
	}

}